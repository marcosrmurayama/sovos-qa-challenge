# Sovos Challenge #


### Challenge 1 - UI ###

Based on the search functionality of the Amazon website describe test scenarios using equivalence class partitioning (ECP) to assure that everything is working as it is expected for an e-commerce web page. After that, choose an automation framework and implement the described test scenarios, you can use any language or framework (**only open-source frameworks**).

* https://www.amazon.com

Comments:

1 - Please start the evaluation by the Testplan created for a better understanding of the tests proposed and how execute them.

2 - As requested, the test cases creation considered the ECP testing technique. The Challenge 1 test cases have been divided into two parts (identified as 
Requiments): 

- Requirement 1 - Valid Partition
		In this section, test cases focused on searching for valid product names.

- Requirement 2 - Invalid Partition
		In this section, test cases focused on searching for invalid product names.

3 - The spreadsheet with all test cases is located in Downloads section.

4 - The automated tests files are wrapped up in a zip file and is also located in Downloads section.

5 - As additional material for analysis, you can find videos of automated tests execution in the following link:

https://drive.google.com/drive/folders/1JI-7mzy_ItH_VtMOAFGlZ-1EnjHf-ZRs?usp=sharing



### Challenge 2 - API ###

Get Notifications: 
* https://api.mocki.io/v1/4862d8e7

You need to test a GET notifications API that retrieves notifications and check that the following requirements are satisfied:

- should return notifications for the following countries: BR, AR
- perPage value should correspond to the number of notifications retrieved
- content of notifications should be a xml encoded on Base64
- notificationId should be a valid GUID
- notificationId should correspond to ID inside content xml document
- 200 notifications should have "Document Authorized" on StatusReason and "Document authorized successfully" on Text fields inside content xml document
- 400 notifications should have "Document Rejected" on StatusReason and "Document was rejected by tax authority" on Text fields inside content xml document
- Automation should display a warn in case of any rejected notification

Describe test scenarios for the given requirements using ECP, after that, choose an automation framework and implement the described test scenarios, you can use any language or framework (**only open-source frameworks**). 

Comments:

1 - Please start the evaluation by the Testplan created for a better understanding of the tests proposed and how execute them.

2 - As requested, the test cases creation considered the ECP testing technique. The Challenge 1 test cases have been divided into two parts (identified as 
Requiments): 

- Requirement 1 - Valid Partition
		In this section, test cases focused on searching for valid product names.

- Requirement 2 - Invalid Partition
		In this section, test cases focused on searching for invalid product names.

3 - The spreadsheet with all test cases is located in Downloads section.

4 - The automated tests files are wrapped up in a Json file and is also located in Downloads section.

5 - As additional material for analysis, you can find screenshots of automated tests execution in the following link:

https://drive.google.com/drive/folders/1BnQFGbYkH2hgF1RGbLegMdpDd79bVFmO?usp=sharing


## Make a fork of this repository and submit a pull request with your solutions! Good luck! ##


**The use of automation design patters will be a differential.**

**Please, include in the README all the information needed to run the project!**

**Don't forget to add all scenarios that you described using ECP in a specific session of your README, this has a lot of weight in the evaluation!!**
